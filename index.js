/* eslint-disable no-console */
"use strict";
const mongoose = require("mongoose");
const app = require("./app");

const config = require("./config");
mongoose.connect(
  config.DB,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  (err) => {
    if (err) {
      throw err;
    }
    app.listen(config.PORT, () => {
      console.log(`Corriendo en http://localhost:${config.PORT}`);
    });
  }
);
