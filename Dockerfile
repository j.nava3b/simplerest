
FROM node:10
ENV DBURL=''
RUN mkdir /api_rest
WORKDIR /api_rest
ADD package.json /api_rest/
COPY . /api_rest/
RUN npm install --production
ADD . /api_rest/
EXPOSE 5555 

# Serve the app
CMD ["npm", "run", "start:prod"]