module.exports = {
  PORT: process.env.PORT || 5555,
  DB: process.env.DBURL || "mongodb://localhost:27017/shop",
  SECRET_TOKEN: "secretshh",
};
/*
docker run --link mongodb2 -e DBURL="mongodb://mongodb2:27017/shop" -p5555:5555 jacknavarow/rest_api:1.0.0 
*/