"use strict";
const service = require("../services");

function isAuth(req, res, next) {
  if (!req.headers.auhorization) {
    return res.status(403).send({ message: " n tienes autorizacion" });
  }
  const token = req.headers.auhorization.split(" ")[1];
  service
    .decodeToken(token)
    .then((data) => {
      req.user = data;
      next();
    })
    .catch((err) => {
      res.status(err.status).send(err.mensaje);
    });
}
module.exports = isAuth;
