const Product = require("../models/products");

function getProduct(req, res) {
  Product.findById(req.params.productId, (err, product) => {
    if (err) res.status(500).json({ message: "Error realizando perticion2" });
    if (!product) res.status(404).json({ message: "Producto no existe" });
    if (!err && product) {
      res.status(200).json({ product });
    }
  });
}

function getProducts(req, res) {
  Product.find({}, (err, products) => {
    if (err) res.status(500).json({ message: "Error realizando perticion" });
    if (!products) res.status(200).json({ message: "Producto no existe" });
    res.status(200).json({ products });
  });
}

function updateProduct(req, res) {
  let productId = req.params.productId;
  let update = req.body;

  Product.findByIdAndUpdate(productId, update, (err, productUpdate) => {
    if (err)
      res.status(500).json({ message: "Error al actualizar el producto" });
    if (productUpdate) res.status(200).json({ product: productUpdate });
  });
}
function deleteProduct(req, res) {
  Product.findByIdAndRemove(req.params.productId, (err, product) => {
    if (err)
      res.status(500).json({ message: "Error al eliminar el producto " + err });
    if (!product) res.status(500).json({ message: "Error al eliminar datos " });
    if (!err && product)
      res.status(200).json({ message: "Producto Eliminado" });
  });
}
function saveProduct(req, res) {
  const product = new Product();
  product.name = req.body.name;
  product.picture = req.body.picture;
  product.category = req.body.category;
  product.price = req.body.price;
  product.description = req.body.description;
  product.save((err, productStored) => {
    if (err) {
      res.status(500).json({ message: "Error al salvar datos " });
    } else {
      res.status(200).json({ product: productStored });
    }
  });
}
module.exports = {
  getProduct,
  getProducts,
  updateProduct,
  deleteProduct,
  saveProduct,
};
