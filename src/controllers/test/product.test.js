"use strict";

const {
  getProducts,
  getProduct,
  saveProduct,
  deleteProduct,
  updateProduct,
} = require("../products");
const Product = require("../../models/products");

describe("Product", () => {
  const productoMock = {
    name: 123,
    picture: 123,
    category: "computers",
    price: 123,
    description: 123,
  };

  it("getProducts", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product, "find")
      .mockImplementation((id, cb) => cb(null, [productoMock]));
    await getProducts({}, mockResponse);
    expect(mockResponse.json).toHaveBeenCalledTimes(1);
    expect(mockResponse.json).toHaveBeenCalledWith({
      products: [productoMock],
    });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(200);
  });
  it("[ERROR] getProducts ", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product, "find")
      .mockImplementation((id, cb) => cb("error", [productoMock]));
    await getProducts({}, mockResponse);
    expect(mockResponse.status).toHaveBeenCalledWith(500);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "Error realizando perticion",
    });
  });
  it("Get a Product", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product, "findById")
      .mockImplementation((id, cb) => cb(null, { ...productoMock, id: 1 }));

    const req = {
      params: { id: 1 },
    };
    await getProduct(req, mockResponse);

    expect(mockResponse.json).toHaveBeenCalledTimes(1);
    expect(mockResponse.json).toHaveBeenCalledWith({
      product: { ...productoMock, id: 1 },
    });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(200);
  });
  it("[ERROR] get a Product ", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product, "findById")
      .mockImplementation((id, cb) => cb("error", [productoMock]));
    await getProduct({ params: { productId: 1 } }, mockResponse);
    expect(mockResponse.status).toHaveBeenCalledWith(500);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "Error realizando perticion2",
    });
  });
  it("Insert", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product.prototype, "save")
      .mockImplementation((cb) => cb(null, { ...productoMock, id: 1 }));
    const req = {
      body: { ...productoMock },
    };
    await saveProduct(req, mockResponse);

    expect(mockResponse.json).toHaveBeenCalledTimes(1);
    expect(mockResponse.json).toHaveBeenCalledWith({
      product: { ...productoMock, id: 1 },
    });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(200);
  });
  it("[ERROR] Insert", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product.prototype, "save")
      .mockImplementation((cb) => cb("error", null));
    const req = {
      body: { ...productoMock },
    };
    await saveProduct(req, mockResponse);
    expect(mockResponse.status).toHaveBeenCalledWith(500);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "Error al salvar datos ",
    });
  });
  it("deleteProduct", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product, "findByIdAndRemove")
      .mockImplementation((id, cb) => cb(null, { ...productoMock, id: 1 }));
    const req = {
      params: { id: 1 },
    };
    await deleteProduct(req, mockResponse);

    expect(mockResponse.json).toHaveBeenCalledTimes(1);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "Producto Eliminado",
    });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(200);
  });
  it("[ERROR] Delete", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product, "findByIdAndRemove")
      .mockImplementation((id, cb) => cb("error", null));
    const req = {
      body: { ...productoMock },
      params: { id: 1 },
    };
    await deleteProduct(req, mockResponse);
    expect(mockResponse.status).toHaveBeenCalledWith(500);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "Error al eliminar el producto error",
    });
  });
  it("UpdateProduct", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const updProd = { ...productoMock };
    updProd.name = "Nuevo Nombre";
    updProd.id = 1;
    jest
      .spyOn(Product, "findByIdAndUpdate")
      .mockImplementation((id, body, cb) => cb(null, { ...updProd }));

    const req = {
      params: { id: updProd.id },
      body: updProd,
    };
    await updateProduct(req, mockResponse);

    expect(mockResponse.json).toHaveBeenCalledTimes(1);
    expect(mockResponse.json).toHaveBeenCalledWith({ product: updProd });

    expect(mockResponse.status).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(200);
  });
  it("[ERROR] Update", async () => {
    const mockResponse = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    jest
      .spyOn(Product, "findByIdAndUpdate")
      .mockImplementation((id, body, cb) => cb("error", null));
    const req = {
      body: { ...productoMock },
      params: { id: 1 },
    };
    await updateProduct(req, mockResponse);
    expect(mockResponse.status).toHaveBeenCalledWith(500);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "Error al actualizar el producto",
    });
  });
});
