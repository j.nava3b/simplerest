"use strict";
const express = require("express");
const ProdCtrl = require("../controllers/products");
const userCrtl = require("../controllers/users");
const auth = require("../middlewares/auth");
const api = express.Router();
api.get("/product", ProdCtrl.getProducts);
api.get("/product/:productId", ProdCtrl.getProduct);
api.post("/product", ProdCtrl.saveProduct);

api.put("/product/:productId", ProdCtrl.updateProduct);
api.delete("/product/:productId", ProdCtrl.deleteProduct);
api.get("/private", auth, (req, res) => {
  res.status(200).send({ message: "tienes acceso" });
});
api.post("/login", userCrtl.signIn);
api.post("/signUp", userCrtl.signUp);

module.exports = api;
